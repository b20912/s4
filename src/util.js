function getCircleArea(radius){
	if(typeof radius !== "number"){
		return undefined;
	}

	if(radius <= 0){
		return undefined;
	}

	return 3.1416*(radius**2);

}

function getNumberOfChar(char,string){

	//transform the string into an array of characters so we can check the given char parameter against each character in the string parameter
	//.split("") splits the characters ina string and retruns them in a new array
	if(typeof char !== 'string' || typeof string !== 'string'){
		return undefined;
	}

	let characters = string.split("");

	let counter = 0;
	characters.forEach(character => {

		if(character === char){
			counter++;
		}
	})
	return counter;
}

let users = [
	{
		username: "brBoyd87",
		password: "87brandon19"
	},
	{
		username: "tylerOfsteve",
		password: "stevenstyle75"
	}
]

module.exports = {
	getCircleArea,
	getNumberOfChar,
	users
}