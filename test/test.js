const {assert} = require('chai');
const {getCircleArea,getNumberOfChar} = require('../src/util.js')

describe('test_get_circle_area',()=>{

	it('test_area_of_circle_radius_15_is_706.86',()=>{

		let area = getCircleArea(15);
		assert.equal(area,706.86);
	})

	it('test_area_of_circle_neg1_radius_is_undefined', () => {
		let area = getCircleArea(-1);
		assert.isUndefined(area);
	})

	it('test_area_of_circle_0_radius_is_undefined',() => {
		let area = getCircleArea(0);
		assert.isUndefined(area);
	})

	it('test_area_of_circle_string_radius_is_undefined',() => {
		let area = getCircleArea("25");
		assert.isUndefined(area);
	})

	it('test_area_of_circle_invalid_type_radius_is_undefined',() => {
		let area = getCircleArea(true);
		assert.isUndefined(area);
	})
})

describe('test_get_number_of_char_in_sentence',()=>{

	it('test_number_of_l_in_sentence_3',()=>{

		let numChar = getNumberOfChar("l","Hello World");
		assert.equal(numChar,3);
	})


	it('test_not_string_char_is_undefined',()=>{

		let numChar = getNumberOfChar(2,"Hello World");
		assert.isUndefined(numChar);
	})

	it('test_not_string_string_is_undefined',()=>{

		let numChar = getNumberOfChar("l",true);
		assert.isUndefined(numChar);
	})
})